require 'test_helper'

class TodoListTest < ActiveSupport::TestCase
  test "todo_list attributes must not be empty" do
    product = TodoList.new
    assert product.invalid?
    assert product.errors[:title].any?
    assert product.errors[:description].any?
  end
 

  test "todo_list is not valid without a unique title" do
    todo_list = TodoList.new(title:       todo_lists(:one).title,
                            description: "yyy")

    assert todo_list.invalid?
    assert_equal ["title has already been taken"], todo_list.errors[:title]
  end

  test "todo_list is not valid without a unique description" do
    todo_list = TodoList.new(title:       "nome",
                            description: todo_lists(:one).description)

    assert todo_list.invalid?
    assert_equal ["description has already been taken"], todo_list.errors[:title]
  end
  
end