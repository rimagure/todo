require 'test_helper'

class TodoItemTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  test "todo_list attributes must not be empty" do
    product = TodoItem.new
    assert product.invalid?
    assert product.errors[:content].any?
  end
 

  test "todo_item is not valid without a unique title" do
    todo_item = TodoItem.new(content: todo_items(:one).content,
    	                     todo_list: todo_items(:one).todo_list)

    assert todo_item.invalid?
    assert_equal ["content has already been taken"], todo_item.errors[:title]
  end

end
