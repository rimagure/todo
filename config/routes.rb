Rails.application.routes.draw do
  
  get 'admin' => 'todo_lists#index'
  controller :sessions do
    get  'login' => :new
    post 'login' => :create
    delete 'logout' => :destroy
  end

  get 'sessions/new'
  get 'sessions/create'
  get 'sessions/destroy'
  resources :users
  resources :todo_lists do
  	resources :todo_items do
  		member do
  			patch :complete
  		end
  	end
end
  
  root "todo_lists#index"

end